public class GoodTurtle extends Turtle{
	
	public void drawTriangle(int size)
	{
		turnRight(30);
		for(int i=0;i<3;i++)
		{
			move(size);
			turnRight(120);
		}
		setDirection(270);
	}
	
	int size = 100;
	public void drawCoolTriangle()
	{
		double trianglePointX = getX();
		double trianglePointY = getY();
		
		coolTrianglePart1();
	}
	
	public void oneCoolTriangle()
	{
		drawTriangle(size/2);
		
		goToSecondTriangle();
		drawTriangle(size/2);
		
		goToThirdTriangle();
		drawTriangle(size/2);
		
		turnRight(270);
		move(size/2);
		turnRight(90);
	}
	
	public void coolTrianglePart1()
	{
		for(int i=0; i<2; i++)
		{
			oneCoolTriangle();
		}
	}
	
	public void goToSecondTriangle()
	{
		turnRight(30);
		move(size/2);
		setDirection(270);
	}
	
	public void goToThirdTriangle()
	{
		turnRight(150);
		move(size/2);
		setDirection(270);
	}
}
