import java.util.ArrayList;

public class Turtle{

	private double direction;
	private double x, y;
	private boolean penDown;
	private Color color;
	private int WIDTH, HEIGHT;
	private int width, height;
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	private ArrayList<TurtlePoint> points;
	
	/**
	 * Default constructor: set direction to 270 (up), x and y to 300, 
	 * penDown to true, and WIDTH and HEIGHT to 600. 
	 */
	public Turtle()
	{
		direction = 270;
		x = 300;
		y = 300;
		penDown = true;
		color = new Color(0,0,0);
		WIDTH = 600;
		HEIGHT = 600;
		width = 10;
		height = 10;
		points = new ArrayList<TurtlePoint>();
		points.add(new TurtlePoint(x, y, penDown, color));
	}
	
	/**
	 * 2-param constructor: set direction to 270 (up), x and y to half
	 * the width and height, respectively, penDown to true, 
	 * and WIDTH and HEIGHT to the given values.
	 */
	public Turtle(int WIN_WIDTH, int WIN_HEIGHT)
	{
		direction = 270;
		x = WIN_WIDTH/2;
		y = WIN_HEIGHT/2;
		penDown = true;
		color = new Color(0,0,0);
		WIDTH = WIN_WIDTH;
		HEIGHT = WIN_HEIGHT;
		width = 10;
		height = 10;
		points = new ArrayList<TurtlePoint>();
		points.add(new TurtlePoint(x, y, penDown, color));
	}
	
	/**
	 * Sets the pen to be in the "up" state
	 */
	public void penUp()
	{
		 penDown = false;
	}
	
	/**
	 * Sets the pen to be in the "down" state
	 */
	public void penDown()
	{
		penDown = true;
	}
	
	//Write getters for penDown, direction, x, y, WIDTH, and HEIGHT
	
	/**
	 * Turns the turtle right a given amount of degrees
	 * @param degrees
	 */
	public void turnRight(int degrees)
	{
		this.direction += degrees;
		this.direction = this.direction % 360;
		if(direction < 0)
		{
			direction += 360;
		}
	}
	
	/**
	 * Turns the turtle left a given amount of degrees
	 * @param degrees
	 */
	public void turnLeft(int degrees)
	{
		turnRight(-degrees);
	}
	
	/**
	 * Moves the turtle the given distance and returns true.
	 * If it cannot move because it would go off screen, it 
	 * should NOT move and return false.
	 * @param distance
	 */
	public boolean move(double distance)
	{
		double dx = distance * Math.cos(Math.toRadians(direction));
		double dy = distance * Math.sin(Math.toRadians(direction));
		x += dx;
		y += dy;
		if(x < 0 || x > WIDTH || y < 0 || y > HEIGHT)
		{
			x -= dx;
			y -= dy;
			return false;
		}
		points.add(new TurtlePoint(x,y,penDown,color));
		return true;
	}
	
	public void jumpTo(double x, double y)
	{
		setX(x);
		setY(y);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public ArrayList<TurtlePoint> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<TurtlePoint> points) {
		this.points = points;
	}

	public void setWIDTH(int wIDTH) {
		WIDTH = wIDTH;
	}

	public void setHEIGHT(int hEIGHT) {
		HEIGHT = hEIGHT;
	}

	public double getDirection() {
		return direction;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public boolean isPenDown() {
		return penDown;
	}

	public int getWIDTH() {
		return WIDTH;
	}

	public void setDirection(double direction) {
		this.direction = direction;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setPenDown(boolean penDown) {
		this.penDown = penDown;
	}

	public int getHEIGHT() {
		return HEIGHT;
	}
	
	public class Color{
		private int r, g, b;
		public Color(int r, int g, int b) {
			this.r = r;
			this.g = g;
			this.b = b;
		}
		public int getR() {
			return r;
		}
		public void setR(int r) {
			this.r = r;
		}
		public int getG() {
			return g;
		}
		public void setG(int g) {
			this.g = g;
		}
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
	}

	public class TurtlePoint{
		double x, y;
		boolean penDown;
		Color c;
		public TurtlePoint(double x, double y, boolean penDown, Color c) {
			this.x = x;
			this.y = y;
			this.penDown = penDown;
			this.c = c;
		}
		public double getX() {
			return x;
		}
		public void setX(double x) {
			this.x = x;
		}
		public double getY() {
			return y;
		}
		public void setY(double y) {
			this.y = y;
		}
		public boolean isPenDown() {
			return penDown;
		}
		public void setPenDown(boolean penDown) {
			this.penDown = penDown;
		}
		public Color getC() {
			return c;
		}
		public void setC(Color c) {
			this.c = c;
		}
	}
}
