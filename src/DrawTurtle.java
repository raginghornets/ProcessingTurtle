import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PShape;

public class DrawTurtle extends PApplet
{
	private static GoodTurtle t;
	private double moveIncrement;
	private int angleIncrement;
	
	public static void main(String[] args) {
		t = new GoodTurtle();
		PApplet.main("DrawTurtle");
	}
	
	public void settings()
	{
		size(t.getWIDTH(),t.getHEIGHT());
	}
	
	public void setup()
	{
		background(255);
		moveIncrement = 10;
		angleIncrement = 10;
		t.drawCoolTriangle();
	}
	
	public void draw()
	{
		background(255);
		fillTurtle();
		drawPoints();
		drawTurtle();
	}

	public void drawPoints()
	{
		for(int i = 0; i < t.getPoints().size() - 1; i++)
		{
			Turtle.TurtlePoint p1 = t.getPoints().get(i);
			Turtle.TurtlePoint p2 = t.getPoints().get(i+1);
			line((float) p1.getX(), (float) p1.getY(), (float) p2.getX(), (float) p2.getY());
		}
	}
	
	public void drawTurtle()
	{
		ellipse((float) t.getX(), (float) t.getY(), 10, 10);
		drawNavigator();
	}
	
	public void fillTurtle()
	{
		int r = t.getColor().getR();
		int g = t.getColor().getG();
		int b = t.getColor().getB();
		fill(r, g, b);
	}
	
	public void drawNavigator()
	{
		float direction = (float) (Math.toRadians(t.getDirection()-270));
		float arcStart = (float) (direction+(4*PI)/3);
		float arcEnd = (float) (direction+(5*PI)/3);
		noFill();
		arc((float) t.getX(), (float) t.getY(), t.getWidth()+10, t.getHeight()+10, arcStart, arcEnd);
	}
	
	public void keyPressed()
	{
		moveW();
	}
	
	public void moveWASD()
	{
		if(key == 'w')
		{
			t.setDirection(270);
			t.move(moveIncrement);
		}
		if(key == 's')
		{
			t.setDirection(90);
			t.move(moveIncrement);
		}
		if(key == 'a')
		{
			t.setDirection(180);
			t.move(moveIncrement);
		}
		if(key == 'd')
		{
			t.setDirection(0);
			t.move(moveIncrement);
		}
	}
	
	public void moveW()
	{
		if(key == 'w')
		{
			t.move(moveIncrement);
		}
		if(key == 's')
		{
			t.move(-moveIncrement);
		}
		if(key == 'a')
		{
			t.turnLeft(angleIncrement);
		}
		if(key == 'd')
		{
			t.turnRight(angleIncrement);
		}
	}
}